#!/usr/bin/python

import socket
import uuid

uuid = str(uuid.uuid4())
xml = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:wsd="http://schemas.xmlsoap.org/ws/2005/04/discovery" xmlns:wsdp="http://schemas.xmlsoap.org/ws/2006/02/devprof"><soap:Header><wsa:To>urn:schemas-xmlsoap-org:ws:2005:04:discovery</wsa:To><wsa:Action>http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</wsa:Action><wsa:MessageID>urn:uuid:'+uuid+'</wsa:MessageID></soap:Header><soap:Body><wsd:Probe><wsd:Types>wsdp:Device</wsd:Types></wsd:Probe></soap:Body></soap:Envelope>'


info = socket.getaddrinfo("FF02::C", 3702, socket.AF_UNSPEC, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s = socket.socket(info[0][0], info[0][1], info[0][2])
s.sendto(xml, info[0][4])

result = s.recv(1500)
print result

