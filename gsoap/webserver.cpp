#include "gen/soapH.h"
#include "httpget.h"
#include <pthread.h>

#define BACKLOG (100)
#define SERVER_TIMEOUT (24*60*60) /* die after 24 hrs waiting */
#define IO_TIMEOUT (60) /* deny serving slow clients */

static int server_port;
static const char *server_uuid;

void *process_webservice(void*);	/* multi-threaded request handler */
void *process_request(void*);	/* multi-threaded request handler */

int http_get_handler(struct soap*);	/* HTTP get handler */
int copy_file(struct soap*, const char*, const char*);	/* copy file as HTTP response */
int wst(struct soap*);

/******************************************************************************\
 *
 *	Main
 *
\******************************************************************************/

int run_webservice(int port, const char *uuid)
{ 
  pthread_t tid;
  server_uuid = uuid;
  server_port = port;
  pthread_create(&tid, NULL, (void*(*)(void*))process_webservice, NULL);
  return 0;
}

void *process_webservice(void *)
{
  struct soap soap, *tsoap;
  pthread_t tid;
  int m, s, i;

  fprintf(stderr, "Starting Web server on port %d\n", server_port);
  fprintf(stderr, "uuid is %s\n", server_uuid);

  soap_init2(&soap, SOAP_IO_KEEPALIVE, SOAP_IO_KEEPALIVE);
  /* Register HTTP GET plugin */
  soap_register_plugin_arg(&soap, http_get, (void*)http_get_handler);
  /* Unix SIGPIPE, this is OS dependent */
#ifdef SO_NOSIGPIPE
  soap.accept_flags = SO_NOSIGPIPE; 	/* some systems like this */
#else
#ifdef MSG_NOSIGNAL
  soap.socket_flags = MSG_NOSIGNAL; 	/* others need this */
#endif
#endif
  m = soap_bind(&soap, NULL, server_port, BACKLOG);
  if (m < 0)
  { soap_print_fault(&soap, stderr);
    exit(1);
  }

  soap.accept_timeout = SERVER_TIMEOUT;
  soap.send_timeout = IO_TIMEOUT;
  soap.recv_timeout = IO_TIMEOUT;
  for (i = 1; ; i++)
  { 
    s = soap_accept(&soap);
    if (s < 0)
    { if (soap.errnum)
      { soap_print_fault(&soap, stderr);
        exit(1);
      }
      fprintf(stderr, "gSOAP Web server timed out\n");
      break;
    }
    fprintf(stderr, "Thread %d accepts socket %d connection from IP %d.%d.%d.%d\n", i, s, (int)(soap.ip>>24)&0xFF, (int)(soap.ip>>16)&0xFF, (int)(soap.ip>>8)&0xFF, (int)soap.ip&0xFF);
    tsoap = soap_copy(&soap);
    pthread_create(&tid, NULL, (void*(*)(void*))process_request, (void*)tsoap);
  }
  soap_done(&soap);
  return 0;
}

/******************************************************************************\
 *
 *	Process dispatcher
 *
\******************************************************************************/

void *process_request(void *soap)
{ 
  pthread_detach(pthread_self());
  soap_serve((struct soap*)soap);
  /* soap_destroy((struct soap*)soap); */ /* cleanup class instances (but this is a C app) */
  soap_end((struct soap*)soap);
  soap_done((struct soap*)soap);
  free(soap);
  return NULL;
}

/******************************************************************************\
 *
 *	Server dummy methods to avoid link errors
 *
\******************************************************************************/

SOAP_FMAC5 int SOAP_FMAC6 SOAP_ENV__Fault(struct soap*, char *faultcode, char *faultstring, char *faultactor, struct SOAP_ENV__Detail *detail, struct SOAP_ENV__Code *SOAP_ENV__Code, struct SOAP_ENV__Reason *SOAP_ENV__Reason, char *SOAP_ENV__Node, char *SOAP_ENV__Role, struct SOAP_ENV__Detail *SOAP_ENV__Detail)
{
        return SOAP_NO_METHOD; 
}


/******************************************************************************\
 *
 *	HTTP GET handler for plugin
 *
\******************************************************************************/

int http_get_handler(struct soap *soap)
{ 
  /* HTTP response choices: */
  /* soap_omode(soap, SOAP_IO_STORE); */ /* you have to buffer entire content when returning HTML pages to determine content length */
  soap_set_omode(soap, SOAP_IO_CHUNK); /* ... or use chunked HTTP content (faster) */
  if (soap->zlib_out == SOAP_ZLIB_GZIP) /* client accepts gzip */
    soap_set_omode(soap, SOAP_ENC_ZLIB); /* so we can compress content (gzip) */
  soap->z_level = 9; /* best compression */
  /* Use soap->path (from request URL) to determine request: */
  fprintf(stderr, "Request: %s\n", soap->endpoint);
  /* Note: soap->path starts with '/' */
  if(soap->path[strlen(soap->path)-1] == '/')
    soap->path[strlen(soap->path)-1] = '\0';
  if (strchr(soap->path + 1, '/') || strchr(soap->path + 1, '\\'))	/* we don't like snooping in dirs */
    return 403; /* HTTP forbidden */
  if (!soap_tag_cmp(soap->path, "*.html"))
    return copy_file(soap, soap->path + 1, "text/html");
  if (!soap_tag_cmp(soap->path, "*.xml")
   || !soap_tag_cmp(soap->path, "*.xsd")
   || !soap_tag_cmp(soap->path, "*.wsdl"))
    return copy_file(soap, soap->path + 1, "text/xml");
  if (!soap_tag_cmp(soap->path, "*.jpg"))
    return copy_file(soap, soap->path + 1, "image/jpeg");
  if (!soap_tag_cmp(soap->path, "*.gif"))
    return copy_file(soap, soap->path + 1, "image/gif");
  if (!soap_tag_cmp(soap->path, "*.ico"))
    return copy_file(soap, soap->path + 1, "image/ico");
  if (!strncmp(soap->path+1, server_uuid, strlen(server_uuid)+1))
    return wst(soap);
  return 404; /* HTTP not found */
}

/******************************************************************************\
 *
 *	Copy static page
 *
\******************************************************************************/

int copy_file(struct soap *soap, const char *name, const char *type)
{ FILE *fd;
  size_t r;
  fd = fopen(name, "r"); /* open file to copy */
  if (!fd)
    return 404; /* return HTTP not found */
  soap->http_content = type;
  if (soap_response(soap, SOAP_FILE)) /* OK HTTP response header */
  { soap_end_send(soap);
    fclose(fd);
    return soap->error;
  }
  for (;;)
  { r = fread(soap->tmpbuf, 1, sizeof(soap->tmpbuf), fd);
    if (!r)
      break;
    if (soap_send_raw(soap, soap->tmpbuf, r))
    { soap_end_send(soap);
      fclose(fd);
      return soap->error;
    }
  }
  fclose(fd);
  return soap_end_send(soap);
}

/**********
 * wst handler
 ***********/

int wst(struct soap *soap)
{
  _wst__Get get;
  _wst__GetResponse getResponse;
  int err;

  printf("Entered wst handler");
 
  err = soap_read__wst__Get(soap, &get);
  if(err != SOAP_OK)
    return err;

  err = soap_send__wst__GetResponse(soap, &getResponse);
 
  return err;
}

