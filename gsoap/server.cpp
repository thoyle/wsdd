/* ---------------------------------------------------------------------------
** This software is in the public domain, furnished "as is", without technical
** support, and with no warranty, express or implied, as to its usefulness for
** any purpose.
**
** server.cpp
** 
** WS-Discovery device
**
** -------------------------------------------------------------------------*/

#include <signal.h>
#include <net/if.h>
#include <ifaddrs.h>

#include "gen/wsdd.nsmap"
#include "wsddapi.h"

int run_webservice(int port, const char *uuid);

#define ADDRESS_V4 "soap.udp://239.255.255.250:3702"
#define ADDRESS_V6 "soap.udp://[FF02::C]:3702"
#define HOST_V4 "239.255.255.250"
#define HOST_V6 "FF02::C"
#define WEBSERVICE_PORT 5357
#define SOAP_PORT 3702
#define DEVICE_TYPE "wsdp:Device pub:Computer"
#define MS_COMPAT_ANONYMOUS "http://www.w3.org/2005/08/addressing/anonymous"

struct __serverdata {
   char endpoint[64];
   char xaddr4[128];
   char xaddr6[128];
} serverdata;

bool stop = false;
void sighandler(int sig)
{
	perror("Signal caught...");
	stop = true;
}

int mainloop(soap* serv)
{		
	return soap_wsdd_listen(serv, -1000000);
}

int getmyaddress(int format, char *buf, int bufsize)
{
	struct ifaddrs *myaddrs, *ifa;
	void *in_addr;
	int res = 0;

	/* TODO: Fix multihomed.  This will probably mean binding to specific interfaces. */
		
	if(getifaddrs(&myaddrs) != 0)
	{
		perror("Unable to get my address");
		exit(1);
	}

	/* Filter out 127.x for v4.  For v6 we do the opposite and only return
	   link locals (which windows uses exclusively in WSD) */
	for(ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next)
	{
		if(ifa->ifa_addr == NULL) continue;
		if(!(ifa->ifa_flags & IFF_UP)) continue;
		if(ifa->ifa_addr->sa_family != format) continue;

	        switch (ifa->ifa_addr->sa_family)
       	 	{
            	case AF_INET:
            	{
               		struct sockaddr_in *s4 = (struct sockaddr_in *)ifa->ifa_addr;
			if(ntohl(s4->sin_addr.s_addr)>>24 == 127) continue;
                	in_addr = &s4->sin_addr;
                	break;
            	}

            	case AF_INET6:
            	{
                	struct sockaddr_in6 *s6 = (struct sockaddr_in6 *)ifa->ifa_addr;
			if(s6->sin6_addr.s6_addr[0] != 0xfe) continue;
			if(s6->sin6_addr.s6_addr[1] != 0x80) continue;
                	in_addr = &s6->sin6_addr;
                	break;
            	}

            	default:
                	continue;
        	}

       		if (!inet_ntop(ifa->ifa_addr->sa_family, in_addr, buf, bufsize))
        	{
            		printf("%s: inet_ntop failed!\n", ifa->ifa_name);
        	}
		else
		{
			res = 1;
			break;
		}
	}
	
	freeifaddrs(myaddrs);
	return res;
}

const char *getxaddr(int format)
{
	char buf[64];
	static char url[128];

	if(!getmyaddress(format, buf, sizeof(buf)))
	{
		printf("Unable to get address\n");
		return NULL;
	}
	if(format == AF_INET6)
		sprintf(url,"http://[%s]:%u/%s/", buf, WEBSERVICE_PORT, serverdata.endpoint+9);
	else
		sprintf(url,"http://%s:%u/%s/", buf, WEBSERVICE_PORT, serverdata.endpoint+9);
	return url;
}

/* Sending the xaddr here is not what windows machines do.. however
   if we try to go through the whole resolve/resolvematches stage the
   windows machine simply drops the resolvematch on the floor.  This
   needs fixing.
 */
void sendHello()
{
	struct soap* soap = soap_new1(SOAP_IO_UDP);
	const char *uuid = soap_wsa_rand_uuid(soap);
	
	int res = soap_wsdd_Hello(soap,
	  SOAP_WSDD_ADHOC,      // mode
	  ADDRESS_V6,         // address of TS
	  uuid,                   // message ID
	  NULL,                 
	  serverdata.endpoint,
	  DEVICE_TYPE,
	  NULL,
	  NULL,
	  serverdata.xaddr6,
          1);
	if (res != SOAP_OK)
		soap_print_fault(soap, stderr);

	res = soap_wsdd_Hello(soap,
	  SOAP_WSDD_ADHOC,      // mode
	  ADDRESS_V4,         // address of TS
	  uuid,                   // message ID
	  NULL,                 
	  serverdata.endpoint,
	  DEVICE_TYPE,
	  NULL,
	  NULL,
	  serverdata.xaddr4,
          1);
	if (res != SOAP_OK)
		soap_print_fault(soap, stderr);

	soap_end(soap);	
}

/*
 * windows machines don't seem to ever send Bye, but it can't hurt
 */
void sendBye()
{
	struct soap* soap = soap_new1(SOAP_IO_UDP);
	const char *uuid = soap_wsa_rand_uuid(soap);

	int res = soap_wsdd_Bye(soap,
	  SOAP_WSDD_ADHOC,      // mode
	  ADDRESS_V6,         // address of TS
	  uuid,                   // message ID
	  serverdata.endpoint,                 
	  NULL,
	  NULL,
	  NULL,
	  NULL,
          1);
	if (res != SOAP_OK)
		soap_print_fault(soap, stderr);

	res = soap_wsdd_Bye(soap,
	  SOAP_WSDD_ADHOC,      // mode
	  ADDRESS_V4,         // address of TS
	  uuid,                   // message ID
	  serverdata.endpoint,                 
	  NULL,
	  NULL,
	  NULL,
	  NULL,
          1);
	if (res != SOAP_OK)
		soap_print_fault(soap, stderr);

	soap_end(soap);
}

void register_mcast(struct soap *serv)
{
	ip_mreq mreq; 
	ipv6_mreq mreq6;

	mreq.imr_multiaddr.s_addr = inet_addr(HOST_V4);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(serv->master, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq))<0) 
	{
		perror("ipv4 group membership failed");
		exit(1);		
	}

	if(!inet_pton(AF_INET6, HOST_V6, &mreq6.ipv6mr_multiaddr))
	{
		printf("Unable to parse host %s\n", HOST_V6);
		exit(1);
	}
	mreq6.ipv6mr_interface = 0;
	
	if (setsockopt(serv->master, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &mreq6, sizeof(mreq6))<0) 
	{
		perror("ipv6 group membership failed");
		exit(1);		
	}
}

int main(int argc, char** argv)
{
	struct soap* serv = soap_new1(SOAP_IO_UDP); 
	serv->bind_flags=SO_REUSEADDR;
	if (!soap_valid_socket(soap_bind(serv, "::", SOAP_PORT, 1000)))
	{
		soap_print_fault(serv, stderr);
		exit(1);
	}

	soap_wsdd_set_InstanceId(time(NULL));

	strcpy(serverdata.endpoint, soap_wsa_rand_uuid(serv));
	strcpy(serverdata.xaddr4, getxaddr(AF_INET));
	strcpy(serverdata.xaddr6, getxaddr(AF_INET6));

	printf("Endpoint is %s\n",serverdata.endpoint);

	run_webservice(WEBSERVICE_PORT, serverdata.endpoint+9);

	register_mcast(serv);	

	sendHello();
	mainloop(serv);

	signal(SIGINT, &sighandler);
	while (!stop)
		mainloop(serv);

	sendBye();
	mainloop(serv);

	return 0;
}

void wsdd_event_ProbeMatches(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, struct wsdd__ProbeMatchesType *matches)
{
}

void wsdd_event_ResolveMatches(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, struct wsdd__ResolveMatchType *match)
{
}

void wsdd_event_Hello(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, const char *EndpointReference, const char *Types, const char *Scopes, const char *MatchBy, const char *XAddrs, unsigned int MetadataVersion)
{
}

void wsdd_event_Bye(struct soap *soap, unsigned int InstanceId, const char *SequenceId, unsigned int MessageNumber, const char *MessageID, const char *RelatesTo, const char *EndpointReference, const char *Types, const char *Scopes, const char *MatchBy, const char *XAddrs, unsigned int *MetadataVersion)
{
}

soap_wsdd_mode wsdd_event_Resolve(struct soap *soap, const char *MessageID, const char *ReplyTo, const char *EndpointReference, struct wsdd__ResolveMatchType *match)
{
	static char LastMessageId[64]="";

	/*
	 * The LastMessageId stuff is a hack since windows machines always
         * reply only once, on the ipv6 interface if it's available.
         */
	if (EndpointReference && (strcmp(EndpointReference, serverdata.endpoint) == 0) )
	{
		printf("wsdd_event_Resolve\tid=%s replyTo=%s endpoint=%s\n", MessageID, ReplyTo, EndpointReference);
	
		if(!strcmp(LastMessageId, MessageID))
		{
			printf("Duplicate message.. ignoring\n");
			return SOAP_WSDD_ADHOC;
		}	
		strcpy(LastMessageId, MessageID);
		const char *uuid = soap_wsa_rand_uuid(soap);
		const char *xaddr;
		if(soap->peer.ss_family == AF_INET6)
		   xaddr = serverdata.xaddr6;
		else
		   xaddr = serverdata.xaddr4;

		if(ReplyTo == NULL)
			ReplyTo = MS_COMPAT_ANONYMOUS;

		soap_wsdd_ResolveMatches(soap, NULL, uuid, MessageID, ReplyTo, EndpointReference, DEVICE_TYPE, NULL, NULL, xaddr, 1);	
	}
	return SOAP_WSDD_ADHOC;
}

soap_wsdd_mode wsdd_event_Probe(struct soap *soap, const char *MessageID, const char *ReplyTo, const char *Types, const char *Scopes, const char *MatchBy, struct wsdd__ProbeMatchesType *matches)
{
	static char LastMessageId[64]="";

	/*
	 * The LastMessageId stuff is a hack since windows machines always
         * reply only once, on the ipv6 interface if it's available.
         */
	const char *uuid = soap_wsa_rand_uuid(soap);
	const char *xaddr;
	if(soap->peer.ss_family == AF_INET6)
	   xaddr = serverdata.xaddr6;
	else
	   xaddr = serverdata.xaddr4;

	if(!strcmp(LastMessageId, MessageID))
	{
		printf("Duplicate message.. ignoring\n");
		return SOAP_WSDD_ADHOC;
	}	
	strcpy(LastMessageId, MessageID);
	if(ReplyTo == NULL)
		ReplyTo = MS_COMPAT_ANONYMOUS;

	printf("wsdd_event_Probe\tid=%s replyTo=%s types=%s scopes=%s\n", MessageID, ReplyTo, Types, Scopes);
	soap_wsdd_init_ProbeMatches(soap,matches);
	soap_wsdd_add_ProbeMatch(soap, matches, serverdata.endpoint, DEVICE_TYPE, NULL, NULL, xaddr, 1);
	soap_wsdd_ProbeMatches(soap, NULL, uuid , MessageID, ReplyTo, matches);
	return SOAP_WSDD_ADHOC;
}

